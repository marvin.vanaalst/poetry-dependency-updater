#!/bin/bash

for project in $(ls -d pip-packages/* poetry-packages/*)
do
    cd $project
    echo "-------------------------------------------------"
    echo $project
    echo "-------------------------------------------------"
    pre-commit install
    echo "pre-commit" && pre-commit run --all
    if [[ `git status --porcelain` ]]; then
        git add .
        echo "commit" && git commit -m "dev: chg: pre-commit"
        echo "pre-commit" && pre-commit run --all
    fi
    cd ../..
done
