#!/bin/bash

if [[ $USER = "ubuntu" ]]; then
    echo "RPI configuration"
    export PATH=/home/ubuntu/.local/bin/:$PATH  # poetry path
    export PATH=/home/ubuntu/miniconda3/bin:$PATH
else
    echo "Work configuration"
    export PATH=/home/marvin/.local/bin/:$PATH  # poetry path
    export PATH=/home/marvin/miniforge3/condabin/:$PATH
fi

# $0 is file path if file is called with absolute path
# dirname gets the containing directory
cd "$(dirname "$0")"

echo "-------------------------------------------------"
echo $(date)

echo ">>> main repo: pull" && git pull --rebase --no-recurse-submodules
echo ">>> main repo: submodule update" && git submodule update --init --recursive
echo ">>> main repo: mamba init" && eval "$(conda shell.bash hook)"
echo ">>> activating environment" && conda activate py311

# pip install --update semver
# python update_all_mamba_envs.py

set +e  # ignore errors
echo ">>> running maturin updates"
for project in $(ls -d maturin-packages/*)
do
    /bin/bash ./update-maturin-dependencies.sh $project
done
set -e  # break on errors again

cd "$(dirname "$0")"
git add poetry-packages
git add pip-packages
# git add maturin-packages

if [[ `git status --porcelain` ]]; then
    echo ">>> updating main repo"
    git commit -m "dev: chg: versions"
    git push --recurse-submodules=no
else
    echo "No changes"
fi

# Send mail
source .dotenv  # gives us USER
CONTENT=$(cat logs/main.log)
DATE="git dependabot logs from $(date "+%Y-%m-%d")"

curl --url "smtps://mail.hhu.de:465" \
    --mail-from "marvin.van.aalst@hhu.de" \
    --mail-rcpt "marvin.vanaalst@gmail.com" \
    --user "$MAIL_USER" \
    -T <(echo -e "From: marvin.van.aalst@hhu.de\nTo: marvin.vanaalst@gmail.com\nSubject: $DATE\n$CONTENT")
