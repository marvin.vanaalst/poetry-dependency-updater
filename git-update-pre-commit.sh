#!/bin/bash

folders=(
    /home/marvin/git
    /home/marvin/git-papers
    # /home/marvin/git-reference
)

# for arg in "$@" ; do
for arg in ${folders[@]}; do
    cd $arg
    for project in $(find . -maxdepth 2 -type f -name '.pre-commit-config.yaml' -printf '%h\n')
    do
        echo "$arg/${project:2}"
        cd "$arg/${project:2}"
        pre-commit autoupdate
        if [[ `git status --porcelain` ]]; then
            echo ">>> adding pre-commit config" && git add .pre-commit-config.yaml
            echo ">>> comitting" && git commit -m "bot: chg: updated pre-commit" --no-verify
        fi
        echo "------"
    done
done



#   - repo: https://github.com/pycqa/flake8
