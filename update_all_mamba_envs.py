from __future__ import annotations

import json
import subprocess


def get_env_names() -> list[str]:
    return json.loads(
        subprocess.run(
            ["mamba", "env", "list", "--json"], stdout=subprocess.PIPE
        ).stdout
    )["envs"]


if __name__ == "__main__":
    for env in get_env_names():
        res = subprocess.run(
            ["mamba", "update", "--yes", "--all", "--prefix", env],
            stdout=subprocess.PIPE,
        )
        stdout = res.stdout.decode()
        print(stdout)

        # For now pin jupyter notebook to < 7
        res = subprocess.run(
            ["mamba", "install", "--yes", "notebook==6.5.6", "--prefix", env],
            stdout=subprocess.PIPE,
        )
        stdout = res.stdout.decode()
        print(stdout)
