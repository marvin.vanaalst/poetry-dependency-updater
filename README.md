# Custom dependency updater

Add new package

git submodule add $git packages/$name
git submodule update --init --recursive


## Schedule

M: update poetry packages
T: update pip packages
W:
T: update maturin dependencies
F:
S:
S: gitlab -> github

```cron
0 3 * * 1 /bin/bash /home/ubuntu/poetry-dependency-updater/update-all-poetry.sh > /home/ubuntu/poetry-dependency-updater/logs/poetry.log 2>&1
# 0 3 * * 2
0 3 * * 3 /bin/bash /home/ubuntu/poetry-dependency-updater/update-all-pip.sh > /home/ubuntu/poetry-dependency-updater/logs/pip.log 2>&1
# 0 3 * * 4
# 0 3 * * 5
# 0 3 * * 6
# 0 3 * * 7
```

