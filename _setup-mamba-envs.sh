export PATH=/home/marvin/conda/bin:$PATH

eval "$(conda shell.bash hook)"

conda activate base
conda config --add channels conda-forge
conda config --set channel_priority strict

conda update -y -n base -c defaults conda
conda install -y mamba

mamba create -y -n py310 python=3.10 ipywidgets=7.7.2 jupyter ipykernel modelbase numpy scipy pandas matplotlib rich tqdm llvmlite
mamba create -y -n py311 python=3.11 ipywidgets=7.7.2 jupyter ipykernel modelbase numpy scipy pandas matplotlib rich tqdm

envs=( "py310" "py311" )
for i in ${envs[@]}; do
    pip install moped seaborn modelbase2 pebble
done

