#!/bin/bash

# exit when any command fails
set -e

project=$1

echo $project

if [[ $USER = "ubuntu" ]]; then
    export PATH=/home/ubuntu/miniconda3/bin:$PATH
else
    export PATH=/home/marvin/miniconda3/condabin/:$PATH
fi

echo ">>> main repo: mamba init" && eval "$(conda shell.bash hook)"
echo ">>> activating environment" && conda activate py311

cd $project
cd $(find . -name "requirements.in" -print0 | xargs -0 -I{} dirname  {})
echo "-------------------------------------------------"
echo $project
echo "-------------------------------------------------"

echo ">>> restore staged" && git restore --staged .
echo ">>> restore" && git restore .
echo ">>> switch main" && git switch main
echo ">>> pull" && git pull --rebase

# Update pre-commit
echo ">>> pre-commit autoupdate" && pre-commit autoupdate
if [[ `git status --porcelain` ]]; then
    echo ">>> adding pre-commit config" && git add .pre-commit-config.yaml
    echo ">>> comitting" && git commit -m "bot: chg: updated pre-commit" --no-verify
fi


# Update dependencies
echo ">>> pip-compile" && uv pip compile --upgrade --generate-hashes requirements.in -o requirements.txt
if [[ `git status --porcelain` ]]; then
    echo "Changes"
    echo ">>> adding requirements" && git add requirements.txt
    echo ">>> comitting" && git commit -m "bot: chg: updated dependencies" --no-verify
    echo ">>> push" && git push -u origin main
else
    echo "No changes"
fi
