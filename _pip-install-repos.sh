#!/bin/bash

# exit when any command fails
set -e
eval "$(conda shell.bash hook)"
conda activate py311
for project in $(ls -d poetry-packages/*)
do
    cd $project
    echo "-------------------------------------------------"
    echo $project
    echo "-------------------------------------------------"
    echo ">>> install" && pip install .
    cd ../..
done
