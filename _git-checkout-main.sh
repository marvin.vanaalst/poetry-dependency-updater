#!/bin/bash

# exit when any command fails
set -e

for project in $(ls -d pip-packages/* poetry-packages/*)
do
    cd $project
    echo "-------------------------------------------------"
    echo $project
    echo "-------------------------------------------------"
    echo ">>> checkout main" && git checkout main
    git branch -vv
    cd ../..
done
