#!/bin/bash

# exit when any command fails
for project in $(ls -d pip-packages/* poetry-packages/*)
do
    cd $project
    echo "-------------------------------------------------"
    echo $project
    echo "-------------------------------------------------"
    git add pyproject.toml .flake8
    git commit -m "dev: chg: unified black settings"
    black .
    git add .
    git commit -m "dev: chg: black"
    cd ../..
done
