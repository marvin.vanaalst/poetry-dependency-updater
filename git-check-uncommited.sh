#!/bin/bash

folders=(
    /home/marvin/git
    /home/marvin/git-papers
    # /home/marvin/git-reference
)

# for arg in "$@" ; do
for arg in ${folders[@]}; do
    cd $arg
    for project in $(ls -d */)
    do
        cd $project
        if [[ `git status --porcelain` ]]; then
            echo "$arg/$project"
            echo "------"
        fi
        cd ..
    done
done
