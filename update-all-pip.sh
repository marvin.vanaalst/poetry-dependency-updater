#!/bin/bash

if [[ $USER = "ubuntu" ]]; then
    echo "RPI configuration"
    export PATH=/home/ubuntu/.local/bin/:$PATH  # poetry path
else
    echo "Work configuration"
    export PATH=/home/marvin/.local/bin/:$PATH  # poetry path
fi

# $0 is file path if file is called with absolute path
# dirname gets the containing directory
cd "$(dirname "$0")"

echo "-------------------------------------------------"
echo $(date)

echo ">>> main repo: pull" && git pull --rebase --no-recurse-submodules
echo ">>> main repo: submodule update" && git submodule update --init --recursive
echo ">>> main repo: mamba init" && eval "$(conda shell.bash hook)"
echo ">>> activating environment" && conda activate py311
echo ">>> updating pre-commit and pip" && pip install --upgrade pre-commit pip

set +e  # ignore errors
echo ">>> running pip updates"
for project in $(ls -d pip-packages/*)
do
    /bin/bash ./update-pip-dependencies.sh $project
done
set -e  # break on errors again

cd "$(dirname "$0")"
git add pip-packages

if [[ `git status --porcelain` ]]; then
    echo ">>> updating main repo"
    git commit -m "dev: chg: versions"
    git push --recurse-submodules=no
else
    echo "No changes"
fi

# Send mail
source .dotenv  # gives us USER
CONTENT=$(cat logs/pip.log)
DATE="git dependabot logs from $(date "+%Y-%m-%d")"

curl --url "smtps://mail.hhu.de:465" \
    --mail-from "aalst@hhu.de" \
    --mail-rcpt "marvin.vanaalst@gmail.com" \
    --user "$MAIL_USER" \
    -T <(echo -e "From: aalst@hhu.de\nTo: marvin.vanaalst@gmail.com\nSubject: $DATE\n$CONTENT")
