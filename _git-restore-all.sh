#!/bin/bash

# exit when any command fails
set -e

for project in $(ls -d pip-packages/* poetry-packages/*)
do
    echo "-------------------------------------------------"
    echo $project && cd $project
    echo "-------------------------------------------------"
    echo ">>> git restore" && git restore .
    cd ../..
done
