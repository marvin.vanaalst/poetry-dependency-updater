#!/bin/bash

# exit when any command fails
set -e

project=$1

echo $project

if [[ $USER = "ubuntu" ]]; then
    export PATH=/home/ubuntu/.local/bin/:$PATH  # poetry path
    export PATH=/home/ubuntu/miniconda3/bin:$PATH
else
    export PATH=/home/marvin/.local/bin/:$PATH  # poetry path
    export PATH=/home/marvin/miniconda3/condabin/:$PATH
fi

echo ">>> main repo: mamba init" && eval "$(conda shell.bash hook)"
echo ">>> activating environment" && conda activate py311

cd $project
echo "-------------------------------------------------"
echo $project
echo "-------------------------------------------------"

echo ">>> restore staged" && git restore --staged .
echo ">>> restore" && git restore .
echo ">>> switch main" && git switch main
echo ">>> pull" && git pull --rebase

# Update pre-commit
echo ">>> pre-commit autoupdate" && pre-commit autoupdate
if [[ `git status --porcelain` ]]; then
    echo ">>> adding pre-commit config" && git add .pre-commit-config.yaml
    echo ">>> comitting" && git commit -m "bot: chg: updated pre-commit" --no-verify
fi


# Update dependencies
echo ">>> poetry update" && poetry update --lock
if [[ `git status --porcelain` ]]; then
    echo "Changes"
    poetry version patch
    VERSION=$(poetry version | awk 'END {print $NF}' | sed -e 's/-alpha./a/' -e 's/-beta./b/')
    BRANCH="v${VERSION}"
    git switch -c $BRANCH

    git add pyproject.toml poetry.lock
    git commit -m "bot: chg: updated dependencies"  --no-verify
    git tag $VERSION  # for correct display in changelog

    gitchangelog > changelog.rst
    git add changelog.rst
    git commit -m "bot: chg: updated changelog"  --no-verify

    echo ">>> push" && git push -u origin $BRANCH \
        -o merge_request.create \
        -o merge_request.merge_when_pipeline_succeeds \
        -o merge_request.remove_source_branch \
        -o merge_request.title=$VERSION \
        -o merge_request.description=$VERSION \
        -o merge_request.assign="marvin.vanaalst"
    git push origin $VERSION  # push the tag
    git switch main
    git branch -D $BRANCH
else
    echo "No changes"
fi


# Additional merge request options, e.g.
# merge_request.remove_source_branch
# https://docs.gitlab.com/ee/user/project/push_options.html
