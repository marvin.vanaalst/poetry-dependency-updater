#!/bin/bash
echo $(date)

folders=(
    /home/marvin/git
    /home/marvin/git-papers
    # /home/marvin/git-reference
)

# for arg in "$@" ; do
for arg in ${folders[@]}; do
    cd $arg
    for project in $(ls -d */)
    do
        echo "$arg/$project"
        cd $project
        git pull --all --rebase --no-recurse-submodules
        git push --recurse-submodules=no
        cd ..
        echo "------"
    done
done
