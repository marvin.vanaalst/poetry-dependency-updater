#!/bin/bash

# set -e

for project in $(ls -d pip-packages/* poetry-packages/*)
do
    cd $project
    git checkout main
    git add .
    if [[ `git status --porcelain` ]]; then
        git commit -m "$1"
    fi
done

# Add to main branch
git commit -am "$1"
