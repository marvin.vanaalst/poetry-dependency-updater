#!/bin/bash
# https://github.com/alexandear/import-gitlab-commits
# go install github.com/alexandear/import-gitlab-commits@latest
cd "$(dirname "$0")"

# Import GITLAB_TOKEN and USER
source .dotenv

export GITLAB_BASE_URL="https://gitlab.com/"
export COMMITTER_NAME="Marvin van Aalst"
export COMMITTER_EMAIL="marvin.vanaalst@gmail.com"
export GITLAB_TOKEN=$GITLAB_TOKEN

$(go env GOPATH)/bin/import-gitlab-commits

cd repo.gitlab.com.marvin.vanaalst


if [[ `git remote show origin` ]]; then
    # branch & ref correctly configured
    git pull --rebase
    git push
else
    git remote add origin git@github.com:marvinvanaalst/gitlab.com-contributions.git
    git branch --set-upstream-to=origin/master master
    git pull --rebase
    git push
fi
