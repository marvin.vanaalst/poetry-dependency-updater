#!/bin/bash

# exit when any command fails
set -e

for project in $(ls -d pip-packages/* poetry-packages/*)
do
    cd $project
    echo "-------------------------------------------------"
    echo $project
    echo "-------------------------------------------------"
    echo ">>> checkout dependencies" && git checkout dependencies
    git branch -vv
    cd ../..
done
