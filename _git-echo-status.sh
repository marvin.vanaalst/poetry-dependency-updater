#!/bin/bash

# exit when any command fails
set -e

for project in $(ls -d pip-packages/* poetry-packages/*)
do
    cd $project
    echo "-------------------------------------------------"
    echo $project
    echo "-------------------------------------------------"
    git status
    cd ../..
done
